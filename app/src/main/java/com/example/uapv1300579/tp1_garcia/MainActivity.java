package com.example.uapv1300579.tp1_garcia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listview = (ListView) findViewById(R.id.ListC);
        final CountryList Country = new CountryList();
        final String[] values = Country.getNameArray();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public  void onItemClick(AdapterView parent, View v, int position, long id) {
                            Intent intent = new Intent(MainActivity.this ,CountryActivity.class);
                          ;
                            String itemClicked = (String)parent.getItemAtPosition(position);

                            intent.putExtra("country", itemClicked);
                            startActivity(intent);
                 }
        });
    }
}