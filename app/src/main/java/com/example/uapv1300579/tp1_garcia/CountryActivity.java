package com.example.uapv1300579.tp1_garcia;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static java.lang.String.valueOf;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        final String country =getIntent().getStringExtra("country");

        Intent intent = getIntent();

        TextView payname = (TextView)findViewById(R.id.namecountry);

        payname.setText(country);



        final CountryList countryList = new CountryList();

        final String imgFile = countryList.getCountry(country).getmImgFile();

        Country pay= countryList.getCountry(country);


        TextView capital= (TextView)findViewById(R.id.capital);

        capital.setText(pay.getmCapital());

        TextView lang= (TextView)findViewById(R.id.langue);

        lang.setText(pay.getmLanguage());


        TextView money = (TextView)findViewById(R.id.money);

        money.setText(pay.getmCurrency());

        TextView popu = (TextView)findViewById(R.id.pop);
        int pop = countryList.getCountry(country).getmPopulation();
        popu.setText(valueOf(pop));

        TextView area = (TextView)findViewById(R.id.sup);
        int are = countryList.getCountry(country).getmArea();
        area.setText(valueOf(are));


        Button save =(Button)findViewById(R.id.save) ;



        Log.d("ADebugTag","Value" + imgFile);

        Resources res = this.getResources();
        int imgID = ((Resources) res).getIdentifier(imgFile,"drawable", getPackageName());

        ImageView imgPays =  (ImageView)findViewById(R.id.PayImage);
        imgPays.setImageResource(imgID);

        save.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                //Récupère les données écrites dans la textView
                EditText cptl = (EditText) findViewById(R.id.capital) ;
                EditText lgue = (EditText) findViewById(R.id.langue) ;
                EditText monai =(EditText) findViewById(R.id.money) ;
                EditText plt = (EditText) findViewById(R.id.pop) ;
                EditText spf = (EditText) findViewById(R.id.sup) ;

                //On caste les donées récupéré en leurs type d'origine
                String capital = cptl.getText().toString() ;
                String langue = lgue.getText().toString() ;
                String monnaie = monai.getText().toString() ;
                int population, superficie ;

                //Permet de vérifier que l'utilisateur n'a pas remplit les données chiffré avec des String
                try {
                    population = Integer.parseInt(plt.getText().toString());
                    superficie = Integer.parseInt(spf.getText().toString()) ;

                    //Permet de sauvegarder les données dans la base de données
                    countryList.getHashMap().put(country, new Country(capital,imgFile, langue, monnaie, population, superficie)) ;

                    //Permet d'afficher ce que l'on écrit.
                    Toast.makeText(CountryActivity.this, "Sauvegarde effectué",
                            Toast.LENGTH_LONG).show();
                }
                //Si les données sont fausses ont l'affiche
                catch(Exception e)
                {
                    //Permet l'affichage de l'erreur
                    Toast.makeText(CountryActivity.this, "Erreur de saisie",
                            Toast.LENGTH_LONG).show();
                }
            } ;
        }) ;

    }

    }

